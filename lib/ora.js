const ora = require('optional')('ora');

module.exports = ora;

if (typeof ora === 'function') {
  const controller = init();

  const Ora = Object.getPrototypeOf(ora()).constructor;
  class CustomOra extends Ora {
    constructor(...args) {
      super(...args);
    }
    start(...args) {
      controller.add(this);
      super.start(...args);

      return this;
    }
    stop(...args) {
      controller.remove(this);
      super.stop(...args);

      return this;
    }
  }

  module.exports = function(opts) {
    return new CustomOra(opts);
  };
}

function init() {
  const instances = [];
  const patch = getPatch(console, 'error');

  return {
    add(spinner) {
      const index = instances.indexOf(spinner);

      if (-1 === index) {
        instances.push(spinner);
        patch();
      }
    },
    remove(spinner) {
      const index = instances.indexOf(spinner);

      if (index !== -1) {
        instances.splice(index, 1);
        patch();
      }
    }
  };

  function getPatch(target, key) {
    const method = target[key];

    const wrapper = function() {
      let i = instances.length;
      while(i--) {
        instances[i].clear();
      }

      method.apply(target, arguments);
    };

    const patch = function() {
      if (instances.length > 0) {
        if (target[key] === method) {
          target[key] = wrapper;
        }
      }
      else {
        if (target[key] === wrapper) {
          target[key] = method;
        }
      }
    };

    return patch;
  }
}
