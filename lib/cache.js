#!/usr/bin/env node

const optional = require('optional');
const config = require('../config');
const path = require('path');
const os = require('os');

const cacheDir = path.resolve(os.homedir(), config.ssdp.cache);

const {
  readdirSync,
  mkdirSync,
  writeFileSync,
  existsSync,
  unlinkSync
} = require('fs');

const handler = {
  get(cache, id) {
    const cacheFile = path.join(cacheDir, `${id}.json`);

    const headers = id in cache ? cache[id] : optional(cacheFile);

    if (headers && headers.EXPIRES - Date.now() > 0) {
      return headers;
    }

    if (existsSync(cacheFile)) unlinkSync(cacheFile);
    delete cache[id];
  },
  set(cache, id, headers) {
    if (!headers || typeof headers['CACHE-CONTROL'] !== 'string') {
      return;
    }

    const maxAge = parseInt(headers['CACHE-CONTROL'].replace(/\D/g, ''));

    if (isNaN(maxAge)) {
      return;
    }

    cache[id] = {
      ...headers,
      EXPIRES: new Date(Date.now() + maxAge * 1000)
    };

    const cacheFile = path.join(cacheDir, `${id}.json`);

    mkdirSync(cacheDir, { recursive: true });
    writeFileSync(cacheFile, JSON.stringify(cache[id]), 'utf-8');
  }
};

module.exports = new Proxy(load({}), handler);

function load(cache) {
  mkdirSync(cacheDir, { recursive: true });

  readdirSync(cacheDir).forEach(cacheFile => {
    const id = path.basename(cacheFile, '.json');

    cache[id] = optional(path.join(cacheDir, cacheFile));
    cache[id].EXPIRES = new Date(cache[id].EXPIRES);

    handler.get(cache, id);
  });

  return cache;
}
