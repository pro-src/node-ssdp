#!/usr/bin/env node

const { Client } = require('node-ssdp');
const config = require('../config');
const { configure, validate } = require('./utils');
const prettyjson = require('optional')('prettyjson');

const stringify = prettyjson ?
  data => prettyjson.render(data) : data => JSON.stringify(data, null, 2);

const cache = require('./cache');
const crypto = require('crypto');

module.exports = configure(ssdp, {
  filter(headers) {
    console.error(stringify(headers));
  }
});

async function ssdp(options) {
  const merged = { ...ssdp.defaults, ...options };
  const { type, timeout, interval, filter } = merged;

  validate({ type });
  validate({ filter }, 'function');

  const client = new Client();
  const services = [];


  return await new Promise((resolve, reject) => {
    let id = -1;
    let stopped = false;

    if (timeout) {
      id = setTimeout(() => {
        id = -1;

        if (!stopped) {
          stopped = true;

          client.stop();
          reject(new Error('SSDP search timed out!'));
        }
      }, timeout);
    }

    const handler = (headers, statusCode, rinfo) => {
      const { USN } = headers;

      if (stopped || services.indexOf(USN) !== -1) {
        return;
      }

      services.push(USN);

      const key = crypto.createHash('sha1').update(USN).digest('hex');
      cache[key] = headers;

      if (filter(headers)) {
        console.error(stringify(headers));

        stopped = true;
        client.stop();

        if (id !== -1) {
          clearTimeout(id);
        }

        resolve(headers);
      }
    };

    Object.keys(cache).forEach(key => handler(cache[key]));
    client.on('response', handler);

    (function search() {
      if (!stopped) {
        client.search(type);

        if (interval) {
          setTimeout(search, interval);
        }
      }
    }());
  });
}
