const util = require('util');
const config = require('../config');

module.exports = { run, validate, configure };

function configure(exports, ...options) {
  validate({ main: exports }, 'function');

  exports.options = Object.assign({}, ...options);

  Object.defineProperty(exports, 'defaults', {
    configurable: false,
    enumerable: true,
    get() {
      const defaults = exports.name in config ? config[exports.name] : {};

      try {
        return Object.assign(defaults, exports.options);
      }
      catch(error) {
        const msg = 'Error(s) occurred when configuring %s.\n\n%s;';

        error.message = util.format(msg, exports.name, error.message);
        throw error;
      }
    }
  });

  return exports;
}

async function run(main, ...args) {
  try {
    return await main(...args);
  }
  catch(exception) {
    console.error(exception);
    process.exit(1);
  }
}

function validate(options, type = 'string') {
  for (let key of Object.keys(options)) {
    const actual = typeof options[key];

    if (actual !== type) {
      const msg = 'Expected %s to be type of %s, got type of %s instead.';
      throw new TypeError(util.format(msg, key, type, actual));
    }
  }
}
