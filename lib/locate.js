#!/usr/bin/env node

const ssdp = require('./ssdp');
const { configure, validate } = require('./utils');

module.exports = configure(locate);

async function locate(uuid = locate.defaults.uuid, options = false) {
  let some;

  if (typeof uuid === 'object' || typeof options === 'string') {
    [uuid, options] = [options, uuid];
  }

  if (typeof options === 'boolean') {
    options = { keyword: options };
  }

  const merged = {
    ...ssdp.defaults,
    ...locate.defaults,
    ...options
  };

  if ('keyword' in options && options.keyword !== false) {
    const keyword = options.keyword !== true ?
      options.keyword : typeof uuid === 'string' ?
      uuid : locate.defaults.keyword;

    validate({ keyword });

    const regex = new RegExp(keyword, merged.exact ? '' : 'i');

    some = headers =>
      Object.keys(headers).some(name => regex.test(headers[name]));
  }
  else {
    validate({ uuid });

    const parts = uuid.replace(/^uuid:/, '').split('::');

    if (parts.length < 2 && 'type' in merged) {
      parts.push(merged.type);
    }
    else if (parts.length > 1) {
      merged.type = parts[1];
    }

    usn = 'uuid:' + parts.join('::');

    if (merged.exact) {
      some = headers => headers.USN.startsWith(usn);
    }
    else {
      const regex = new RegExp(usn, 'i');
      some = headers => regex.test(headers.USN);
    }
  }

  if (!('filter' in options)) {
    merged.filter = headers => headers.LOCATION && some(headers);
  }

  options = {};
  for (let key in ssdp.defaults) {
    options[key] = merged[key];
  }

  return (await ssdp(options)).LOCATION;
}
