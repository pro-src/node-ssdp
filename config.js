module.exports = {
  ssdp: {
    cache: '.ssdp-cache',
    type: 'urn:schemas-upnp-org:service:AVTransport:1',
    timeout: 30000,
    interval: 3000
  },
  locate: {
    keyword: 'MicroStack',
    uuid: process.env.XBOX  == '1' ?
     '00863d8b-fce8-41b7-b978-9898734b4d2c' :
     'a4bb786c-74df-8402-fbda-feeb3c26d40d',
    exact: true
  }
};
