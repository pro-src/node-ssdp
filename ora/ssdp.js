#!/usr/bin/env node

const ssdp = require('../lib/ssdp');
const ora = require('../lib/ora');

if (typeof ora !== 'function') {
  module.exports = ssdp;
}
else {
  module.exports = async function wrapper() {
    const spinner = ora('Discovering').start();
    let result;

    try {
      result = await ssdp.apply(this, arguments);
    }
    catch(error) {
      spinner.fail('SSDP discovery failed');
      throw error;
    }

    spinner.succeed('Device(s) discovered');
    return result;
  };
}
