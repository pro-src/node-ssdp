#!/usr/bin/env node

const locate = require('../lib/locate');
const ora = require('../lib/ora');

if (typeof ora !== 'function') {
  module.exports = locate;
}
else {
  module.exports = async function wrapper() {
    const spinner = ora('Locating').start();
    let result;

    try {
      result = await locate.apply(this, arguments);
    }
    catch(error) {
      spinner.fail('Failed to locate device!');
      throw error;
    }

    spinner.succeed('Located');
    return result;
  };
}
