#!/usr/bin/env node

const locate = require('../ora/locate');
const { run } = require('../lib/utils');

const done = location => {
  console.log(location);
  process.exit(0);
};

if (process.argv.length > 2) {
  const keyword = process.argv.slice(2).join(' ');

  run(locate, { keyword }).then(done);
}
else {
  run(locate).then(done);
}

