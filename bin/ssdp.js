#!/usr/bin/env node

const ssdp = require('../ora/ssdp');
const { run } = require('../lib/utils');

run(ssdp, { timeout: false });
